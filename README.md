# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | N         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | N        |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    按Brush可以畫畫，width可以調整大小，width下面有一個調色盤可以調整顏色
    Reset重置畫布，erase是橡皮擦，也可以用width調整大小
    circle、rectangle、triangle可以畫圖形，Fill會決定圖形會不會填滿
    undo、redo可以回到上一步、下一步
    download下載圖片
    
### Function description

    Decribe your bouns function and how to use it.

### Gitlab page link

    your web page URL, which should be "https://108062237.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>
