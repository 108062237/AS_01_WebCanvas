var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
var draw = false;
var mode = 1;
var is_fill = 0;
var init_x,init_y;
var last_x,last_y;
var line_width = 1;
var R = 0;
var G = 0;
var B = 0;
let state = ctx.getImageData(0, 0, 600, 400);
window.history.pushState(state, null);
function mousedown(e){
    ctx.moveTo(e.x,e.y);
    init_x = e.x;
    init_y = e.y;
    draw = true;
    ctx.beginPath();
    if(mode == 1 || mode == 5){
        ctx.lineCap = "round";
        ctx.lineJoin = "round";
    }
    else{
        ctx.lineCap = "square";
        ctx.lineJoin = "square";
    }
    
    

    if(draw){
        if(mode == 1){

            ctx.moveTo(e.x-10,e.y-10);
            ctx.lineTo(e.x-10,e.y-10);
            ctx.stroke();
            
        }
        if(mode == 2){
            
        }
        if(mode == 5){
            ctx.globalCompositeOperation="destination-out";            
            ctx.lineTo(e.x-10,e.y-10);
            ctx.stroke();
        }
    }
}

function mousemove(e){
    //ctx.lineWidth = line_width;
    var W = document.getElementById("width").value;
    ctx.lineWidth = W;
    
    ctx.strokeStyle = document.getElementById("color").value;
    
    if(draw){
        if(mode != 5 ){
            ctx.globalCompositeOperation="source-over";
        }
        if(mode == 1){
            ctx.lineTo(e.x-10,e.y-10);
            ctx.stroke();
        }
        if(mode == 2){
            
            
        }
        if(mode == 5){
            ctx.globalCompositeOperation="destination-out";            
            ctx.lineTo(e.x-10,e.y-10);
            ctx.stroke();
        }
        
    }
}

function mouseup(e){
    
    if(mode == 2){
        ctx.rect(init_x-10, init_y-10, e.x - init_x, e.y - init_y);
        ctx.stroke();
    }
    if(mode == 3){
        var lx,rx,ly,ry;
        if(e.x>init_x){
            rx = e.x;
            lx = init_x;
        }
        else {
            lx = e.x;
            rx = init_x;
        }
        if(e.y>init_y){
            ry = e.y;
            ly = init_y;
        }
        else {
            ly = e.y;
            ry = init_y;
        }
        ctx.ellipse((lx + rx)/2-10,(ly + ry)/2-10,(rx - lx)/2,(ry - ly)/2,0,0,2*Math.PI);
        ctx.stroke();
    }
    if(mode == 4){
        ctx.moveTo((e.x + init_x)/2-10,init_y-10);
        ctx.lineTo(init_x-10,e.y-10);
        ctx.lineTo(e.x-10,e.y-10);
        ctx.closePath();
        ctx.stroke();
    }
    
    if(is_fill && mode !== 1 &&mode !== 5){
        ctx.fillStyle = document.getElementById("color").value;
        ctx.fill();
    }
    
    draw = false;
    
    ctx.closePath();
    state = ctx.getImageData(0, 0, 600, 400);
    window.history.pushState(state, null);
}

function reset(){
    ctx.clearRect(0, 0, 600, 400);
    state = ctx.getImageData(0, 0, 600, 400);
    window.history.pushState(state, null);
}

function brush(){
    mode = 1;
    document.body.style.cursor = "default";
}

function rectangle(){
    mode = 2;
    document.body.style.cursor = "crosshair";
}

function circle(){
    mode = 3;
    document.body.style.cursor = "crosshair";
}

function triangle(){
    mode = 4;
    document.body.style.cursor = "crosshair";
}

function fill(){
    is_fill = !is_fill;
}

function erase(){
    mode = 5;
    document.body.style.cursor = "pointer";
}

window.addEventListener('popstate', changeStep, false);
function changeStep(e){
    ctx.clearRect(0, 0, 600, 400);
    if( e.state ){
        ctx.putImageData(e.state, 0, 0);
    }    
}
function undo(){
    window.history.go(-1);
}
function redo(){
    window.history.go(1);
}



function doInput(id){
    var inputObj = document.createElement('input');
    inputObj.addEventListener('change',readFile,false);
    inputObj.type = 'file';
    inputObj.accept = 'image/*';
    inputObj.id = id;
    inputObj.click();
}
function readFile(){
    var file = this.files[0];
    if(!/image\/\w+/.test(file.type)){
        alert("false");
        return false;
    }
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function(e){
            drawToCanvas(this.result);
    }
}

function drawToCanvas(imgData){
    var cvs = document.querySelector('#cvs');
        cvs.width=300;
        cvs.height=400;
        var ctx = cvs.getContext('2d');
        var img = new Image;
            img.src = imgData;
            img.onload = function(){
                ctx.drawImage(img,0,0,300,400);
                strDataURI = cvs.toDataURL();
            }
}
    